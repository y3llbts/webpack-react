import React from "react";
import ReactDOM from "react-dom";

import "./styles.scss";

import Header from "./components/Header/Header";

var mountNode = document.getElementById("root");

ReactDOM.render(<Header />, mountNode);